const fs = require('fs')

const path = require('path')

function writeData(fName, data) {
    return new Promise((res, rej) => {
        fs.writeFile(`${fName}`, JSON.stringify(data, null, 3), (err) => {
            if (err) {
                rej("Errors encountered ", err)
            }
            else {
                res(`${fName} File Created`)
            }
        })
    })
}

function readData(fName) {
    return new Promise((res, rej) => {
        fs.readFile(fName, "utf-8", (err, data) => {
            if (err) {
                rej("Errors encountered in readData ", err)
            }
            else {
                res(data)
            }
        })
    })
}


function deleteFile(fName) {
    return new Promise((res, rej) => {
        fs.rm(fName, (err) => {
            if (err) {
                rej("Errors in deleting files ", err)
            }
            else {
                res(`${fName} File Deleted`)
            }
        })
    })
}




function addFileName(fName) {
    fs.appendFile('./fileName.txt', fName + ' ', (err) => {
        if (err) {
            console.log(err)
        }
    })

}


function fsProblem2() {
    readData("./data/fs-callbacks/lipsum.txt")
        .then((data) => {
            addFileName("upperCase.txt")
            return writeData("upperCase.txt", data.toUpperCase())
        })
        .then((res) => {
            console.log(res)
            return readData("upperCase.txt")
        })
        .then((uData) => {
            console.log(uData)
            let lowerCase = uData.toLowerCase().match(/[^\.!\?]+[\.!\?]+/g)
            addFileName("lowerCase.txt")
            return writeData("lowerCase.txt", lowerCase)
        })
        .then((res) => {
            console.log(res)
            return readData("lowerCase.txt")
        })
        .then((lData) => {
            let sData = JSON.parse(lData).sort()
            console.log(sData)
            addFileName("sortedData.txt")
            return writeData("sortedData.txt", sData)
        })
        .then((res) => {
            console.log(res)
            return readData("sortedData.txt")

        })
        .then((sData) => {
            console.log(sData)
            return readData("fileName.txt")
        })
        .then((list) => {
            console.log(list)
            return Promise.all([deleteFile("upperCase.txt"), deleteFile("lowerCase.txt"), deleteFile("sortedData.txt")])
        })
        .then((data) => {
            console.log(data)
            return deleteFile('fileName.txt')
        })
        .then((res) => {
            console.log(res)
        })
        .catch((err) => {
            console.error(err)
        })

}


module.exports = fsProblem2;








