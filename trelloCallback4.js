const getData = require('./trelloCallback1')
const getList = require('./trelloCallback2')
const getCard = require('./trelloCallback3')


function getBoardData(boardId, pName) {
    setTimeout(() => {
        getData(boardId)
            .then((bData) => {

                return getList(bData[0].id)
            })
            .then((lData) => {

                let fData = lData.filter((ele) => {
                    return ele["name"] === pName
                })

                return fData[0]
            })
            .then((cData) => {

                return getCard(cData["id"])
            })
            .then((ans) => {
                console.log(ans)
            })
            .catch((err) => console.log(err))
    }, 2 * 1000)

}



module.exports = getBoardData;