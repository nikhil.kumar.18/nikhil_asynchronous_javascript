const fs = require('fs')
const path = require('path')




//creates Directory and also dumps files


function createDir(){
    return new Promise((res,rej)=>{
        fs.mkdir(path.join(__dirname,'tempDir'),(err)=>{
            if(err){
                rej("Error in creating folder")
            }
            else{
                res("Folder Created")
            }
        })
    })
    
}

function createFiles(fName){
    return new Promise((res,rej)=>{
        fs.writeFile(`./tempDir/${fName}`,`${fName} Data`,(err)=>{
            if(err){
                rej("Error in creating file")
            }
            else{
                res("File created")
            }
        })
    })
}


function deleteFilesAndDir() {


    return new Promise((res,rej)=>{
        fs.rm(`tempDir`, { recursive: true }, (err) => {
            if (err) {
                rej("Error in deleting files")
            }
            else{
                res("Files and folder deleted")
            }
        })
    })

    


}



function fsProblem1(){
    Promise.all([createDir(), createFiles("file1.json"), createFiles("file2.json")])
    .then((res) => {
        console.log(res)
        return new Promise((res, rej) => {
            fs.readdir('tempDir', "utf-8", (err, list) => {
                if (err) {
                    rej(err)
                }
                else {
                    console.log("List of files created")
                    res(list)
                }
            })
        })

    })
    .then((list) => {
        console.log(list)
        return deleteFilesAndDir()
    })
    .then((res) => {
        console.log("Deleting files and folder")
        console.log(res)
    })
    .catch((err) => {
        console.error(err)
    })

}


module.exports =fsProblem1;


// async function fsProblem1(){
// try{
//     const createData=await Promise.all([createDir(), createFiles("file1.json"), createFiles("file2.json")])
//     console.log(createData)
//     const delData=await deleteFilesAndDir()
//     console.log(delData)
// }
// catch(err){
//     console.error(err)
// }
// }