
const getData = require('./trelloCallback1')
const getList = require('./trelloCallback2')
const getCard = require('./trelloCallback3')


function getBoardData2(boardId, pName1, pName2) {
    setTimeout(() => {
        getData(boardId)
            .then((bData) => {

                return getList(bData[0].id)
            })
            .then((lData) => {

                let fData = lData.filter((ele) => {
                    return ele["name"] === pName1 || ele["name"] === pName2
                })

                return fData
            })
            .then((cData) => {
               return Promise.all([cData[0],cData[1]])


            }).then((finalData)=>{
                return Promise.all([getCard(finalData[0].id),getCard(finalData[1].id)])
            })
            .then((cData)=>{
                console.log(cData)
            })
            .catch((err) => console.log(err))
    }, 2 * 1000)

}



module.exports = getBoardData2;