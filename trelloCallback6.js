const getData = require('./trelloCallback1')
const getList = require('./trelloCallback2')
const getCard = require('./trelloCallback3')


function getBoardData3(boardId) {
    setTimeout(() => {
        getData(boardId)
            .then((bData) => {

                return getList(bData[0].id)
            })

            .then((cData) => {
                cData.map(async (ele) => {
                    try {
                        const val = await getCard(ele["id"]);
                        console.log(val);
                    }
                    catch (err) {
                        console.error(err)
                    }

                })


            })
            .catch((err) => console.log(err))
    }, 2 * 1000)
}



module.exports = getBoardData3;