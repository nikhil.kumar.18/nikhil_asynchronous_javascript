const fs = require('fs');
const data = require('./data/trello-callbacks/boards.json');

async function getData(id) {


    return new Promise((res, rej) => {
        setTimeout(() => {
            const bData = data.filter((board) => board["id"] === id)
            if (bData.length != 0) {

                res(bData)
            }
            else {
                rej("Not found")
            }
        }, 2 * 1000)

    })

}


// getData("abc122dc")




module.exports = getData;