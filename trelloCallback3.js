const card = require("./data/trello-callbacks/cards.json")

function getCard(id) {
    return new Promise((res, rej) => {
        setTimeout(() => {
            if (card[id]) {
                res(card[id])
            }
            else {
                rej("Not Found");
            }
        }, 2 * 1000)
    })
}

module.exports = getCard;