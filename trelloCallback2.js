const data = require("./data/trello-callbacks/lists.json")

function getList(id) {
    return new Promise((res, rej) => {
        setTimeout(() => {
            if (data[id]) {
                res(data[id])
            }
            else {
                rej(data[id])
            }
        }, 2 * 1000)
    })
}


module.exports = getList;